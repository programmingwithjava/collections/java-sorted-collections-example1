import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class Basket {
    private final String name;
    private final Map<StockItem, Integer> list;

    public Basket(String name) {
        this.name = name;
        /*
        * observe the output of the program with HashMap and TreeMap.
        * in the case of TreeMap you will see that STockItem.compareTo method will be called whenever we
        * want to add a new item to the basket. this is because TreeMap is automatically sorting the items as we
        * add them. this will create performance problems while adding items to TreeMap comparet to adding items
        * to HashMap where items won't be sorted while adding.
        *
        * so there is a cost if you want to have the items in your collection to be ordered, and that is
        * something you need to consider when trying to decide between using a tree map or one of the unordered
        * map classes like HashMap and LinkedHashMap.
        *
        * remember in the case of HashMap insertion is "chaotic", while in the case of LinkedHashMap items are ordered
        * in the way they are added. there is no ordering in any of them though like it is the case with TreeMap.
        * so insertion to HashMap and LinkedHashMap will be always faster then TreeMap.
        * */
        //list = new HashMap<>();
        list = new TreeMap<>();
    }

    public int addToBasket(StockItem item, int quantity){
        if((item != null) && (quantity > 0)){
            /*
            * if the item is already in the list (remember "item" of type StockItem is the key here)
            * return the value for "item" which is the quantity of that item, if not, return 0.
            * */
            int inBasket = list.getOrDefault(item, 0);
            list.put(item, inBasket + quantity);
            return quantity + inBasket;
        }
        return 0;
    }

    public Map<StockItem,Integer> getItems(){
        return Collections.unmodifiableMap(list);
    }

    @Override
    public String toString() {
        String s = "Basket " + name + " has " + list.size() + " item(s).\n";
        double totalCost = 0.0d;
        for(Map.Entry<StockItem, Integer> item : list.entrySet()){
            s+= item.getKey() + ". " + item.getValue() + " purchased.\n";
            totalCost += item.getKey().getPrice()*item.getValue();
        }
        return s + " Total cost is: " + totalCost;
    }
}
