import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class StockList {
    private final Map<String, StockItem> list;

    public StockList() {
        //initialize the final property defined above
        /*
        * when we use LinkedHashMap items will be stored in the way they are put in.
        * change the LinkedHashMap to HashMap and see how the storing order of items in the map becomes chaotic.
        * */
        this.list = new LinkedHashMap<>();
    }

    public int addItem(StockItem item){
        if(item != null){
            /*
            * check if we already have this item in the list
            * if already in the list, return the item from the list, if not
            * assign "item" to "inStock".
            * */
            StockItem inStock = list.getOrDefault(item.getName(), item);
            if(inStock != item){//if the item is already in the list, update quantity
                item.adjustStock(inStock.getQuantityStock());
            }
            /*
            * //alternative to the above approach
            * StockItem inStock = list.get(item.getName());
            * if(inStock != null){
                item.adjustStock(inStock.getQuantityStock());
            }
            * */

            /*
            * if item already exist, put method will override it.
            * if it does not exist, we will insert the new item into the list (HashMap)
            *
            * since we are using the "name" property of the StockItem class in the overridden equals and hasCode methods,
            * it will be appropriate to use the "name" as the key while inserting into the HashMap "list"
            * */
            list.put(item.getName(), item);
            return item.getQuantityStock();
        }else{
            return 0;
        }
    }

    public int sellItem(String itemName, int quantity){
        StockItem inStock = list.getOrDefault(itemName,null);
        if(inStock != null && inStock.getQuantityStock() >= quantity && quantity > 0){
            inStock.adjustStock(-quantity);
            return quantity;
        }
        return 0;
    }

    public StockItem getStockItem(String key){
        return list.get(key);
    }

    public Map<String,StockItem> getStockItems(){
        /*
        * return a map that cannot be modified. we do this to provide immutable classes (along with other things of course).
        * so that other code calling this mmethod cannot make modifications in our "list" map.
        * we could have done the same thing by creating and returning a new instance of HashMap but that would be slower because
        * of copying the entire map (which actually hold references to the StockItem objects,
        * not copies of objects themselves. remember.).
        * */
        return Collections.unmodifiableMap(list);//just a view into the underlying collections. items in the same order.
    }

    @Override
    public String toString() {
        String s = "\nStockList:\n";
        double totalCost = 0.0;
        for(Map.Entry<String, StockItem> item : list.entrySet()){
            StockItem stockItem = item.getValue();

            double itemValue = stockItem.getPrice() * stockItem.getQuantityStock();
            s += stockItem + " There are " + stockItem.getQuantityStock() + " items in stock. value of items: " + itemValue + "\n";
            totalCost += itemValue;
        }
        s += " Total stock value: " + String.format("%.2f",totalCost);
        return s;
    }
}
