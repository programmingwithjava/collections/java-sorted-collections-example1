public class Main {
    private static StockList stockList = new StockList();
    public static void main(String[] args) {

        StockItem item = new StockItem("case", 30.0, 100);
        stockList.addItem(item);

        item = new StockItem("graphics card", 60.0, 100);
        stockList.addItem(item);

        item = new StockItem("keyboard", 15.0, 100);
        stockList.addItem(item);

        item = new StockItem("keyboard", 15.0, 100);
        stockList.addItem(item);

        item = new StockItem("monitor", 100.0, 100);
        stockList.addItem(item);

        item = new StockItem("motherboard", 50.0, 100);
        stockList.addItem(item);

        item = new StockItem("mouse", 10.0, 100);
        stockList.addItem(item);

        item = new StockItem("power supply", 20.0, 100);
        stockList.addItem(item);

        item = new StockItem("SSD disk", 170.0, 100);
        stockList.addItem(item);

        System.out.println(stockList);

        Basket serdarsBasket = new Basket("serdarsBasket");
        sellItem(serdarsBasket, "keyboard", 1);
        sellItem(serdarsBasket, "monitor", 2);
        sellItem(serdarsBasket,"mouse",2);
        sellItem(serdarsBasket,"car",1);
        sellItem(serdarsBasket, "keyboard",2);

        System.out.println(serdarsBasket);
        System.out.println(stockList);


        /*
        * below will throw "UnsupportedOperationException" which is good and what we desire actually.
        * our getStockItems method is returning an unmodifiable map.
        * this is a good security practice to make sure that there is no other way to add an item into our
        * except for addItem method. comment out and execute the code to see the result.
        * */
        //item = new StockItem("cooler", 33, 20);
        //stockList.getStockItems().put("cooler", item);

        /*
        * although the map itself is unmodifiable -as you see above- individual items can be modified as
        * you can see below. it is the collection itself that is unmodifiable, not the objects pointed to.
        *
        * as long as you provide a method that returns a list of items in the collection modifying items will be
        * possible. to avoid this, you could consider writing a method that returns an
        * "unmodifiablemap" of price/item name (double/string).
        * both String and Double are immutable. so not only the returned map will be unmodifiable, but also the
        * individual entries in it.
        * */
        stockList.getStockItems().get("monitor").adjustStock(1000);
        stockList.getStockItem("monitor").adjustStock(1000);//observe the 2 ways of getting an item here
        System.out.println(stockList);
    }

    public static int sellItem(Basket basket, String item, int amount){
        StockItem stockItem = stockList.getStockItem(item);
        if(stockItem == null){
            System.out.println("there is no " + item + " in the inventory.");
            return 0;
        }
        if(stockList.sellItem(item, amount)!=0){
            basket.addToBasket(stockList.getStockItem(item), amount);
            return amount;
        }

        return 0;
    }
}
